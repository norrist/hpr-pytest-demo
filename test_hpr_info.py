import hpr_info


def test_hpr_feed_url():
    assert hpr_info.HPR_FEED == "https://hackerpublicradio.org/hpr_ogg_rss.php"


def test_get_show_data():
    show_data = hpr_info.get_show_data()
    assert show_data.status == 200


def test_get_latest_entry():
    latest_entry = hpr_info.get_latest_entry()
    assert latest_entry["title"]
    assert latest_entry["published"]


def test_get_entry_data():
    entry_data = hpr_info.get_entry_data(hpr_info.get_latest_entry())
    assert entry_data["title"]
    assert entry_data["host"]
    assert entry_data["published"]
    assert entry_data["file"]
