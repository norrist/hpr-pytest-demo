import feedparser

HPR_FEED = "https://hackerpublicradio.org/hpr_ogg_rss.php"


def get_show_data():
    showdata = feedparser.parse(HPR_FEED)
    return showdata


def get_latest_entry():
    showdata = get_show_data()
    return showdata["entries"][0]


def get_entry_data(entry):
    for link in entry["links"]:
        if link.get("rel") == "enclosure":
            enclosure = link.get("href")

    return {
        "title": entry["title"],
        "host": entry["authors"][0]["name"],
        "published": entry["published"],
        "file": enclosure,
    }


if __name__ == "__main__":
    most_recent_show = get_entry_data(get_latest_entry())
    print()
    print("Most Recent HPR Episode Info")
    for x in most_recent_show:
        print(f"{x}: {most_recent_show.get(x)}")
